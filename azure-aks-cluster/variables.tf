variable "resourceGroup" {
  type        = string
  description = "Type a name for the new Resorce Group that will contain the AKS Cluster."
  default = "aria-resourceGroup"
}

variable "clusterName" {
  type        = string
  description = "Type a name for the new AKS Cluster."
  default     = "aria-aks-01"
}

variable "region" {
	type				= string
  description = "Choose on which Azure region you want to provision the new AKS Cluster."
  default     = "eastus"
}

variable "dnsPrefix" {
  type        = string
  description = "DNS name prefix to use with the hosted Kubernetes API server FQDN. You will use this to connect to the Kubernetes API when managing containers after creating the cluster."
  default = "aria"
}

variable "kubernetesVersion" {
  type        = string
  description = "Choose the desired Kubernetes version to use."
  default = "1.25.6"
}

variable "nodeSize" {
  type        = string
  description = "Choose the size of the nodes."
  default = "Standard_DS2_v2"
}

variable "nodeCount" {
  type        = number
  description = "Choose the number of nodes in the cluster."
  default = 2
}
