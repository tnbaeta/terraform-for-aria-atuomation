###########################################################################
# Terraform required providers                                            #
###########################################################################
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.65.0"
    }
  }
}

provider "aws" {
  region = var.region
}

data "aws_availability_zones" "available" {}

###########################################################################

###########################################################################
# IAM Roles and policies for EKS Cluster                #
###########################################################################

data "aws_iam_policy_document" "aria_eks_assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "aria_eks_iam_role" {
  name               = var.iam_role_name
  assume_role_policy = data.aws_iam_policy_document.aria_eks_assume_role.json
}

resource "aws_iam_role_policy_attachment" "aria_eks_cluster_policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.aria_eks_iam_role.name
}

resource "aws_iam_role_policy_attachment" "aria_eks_vpc_resource_controller_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.aria_eks_iam_role.name
}


###########################################################################

###########################################################################
# IAM Roles and policies for Worker Gropus                                #
###########################################################################
data "aws_iam_policy_document" "aria_ec2_assume_role" {
  version = "2012-10-17"
  statement {
    effect = "Allow"

    principals {
      type = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
      
      actions = ["sts:AssumeRole"]
  }
}


resource "aws_iam_role" "aria_eks_node_iam_role" {
  name = var.eks_node_iam_role_name
  assume_role_policy = data.aws_iam_policy_document.aria_ec2_assume_role.json

}

resource "aws_iam_role_policy_attachment" "aria_eks_worker_node_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.aria_eks_node_iam_role.name
}

resource "aws_iam_role_policy_attachment" "aria_eks_cni_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.aria_eks_node_iam_role.name
}

resource "aws_iam_role_policy_attachment" "aria_ec2_container_registry_read_only" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.aria_eks_node_iam_role.name
}
###########################################################################




###########################################################################
# New VPC and subnet settings for EKS cluster                             #
###########################################################################
resource "aws_vpc" "aria_eks_vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = var.vpc_name
  }
}

resource "aws_subnet" "aria_eks_subnet" {
  count = 2

  availability_zone       = data.aws_availability_zones.available.names[count.index]
  cidr_block              = "10.0.${count.index}.0/24"
  vpc_id                  = aws_vpc.aria_eks_vpc.id
  map_public_ip_on_launch = true

  tags = tomap(
    {
      "Name" = "subnet0${count.index}",
      "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared",
    }
  )
}

resource "aws_internet_gateway" "aria_eks_internet_gw" {
  vpc_id = aws_vpc.aria_eks_vpc.id

  tags = {
    Name = "aria-eks_internet_gw"
  }
}

resource "aws_route_table" "aria_eks_route_table" {
  vpc_id = aws_vpc.aria_eks_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.aria_eks_internet_gw.id
  }
}

resource "aws_route_table_association" "aria_eks_route_table_association" {
  count          = 2
  subnet_id      = aws_subnet.aria_eks_subnet.*.id[count.index]
  route_table_id = aws_route_table.aria_eks_route_table.id
}
###########################################################################

###########################################################################
# New EKS Cluster and settings                                            #
###########################################################################
resource "aws_eks_cluster" "aria_eks_cluster" {
  name     = var.eks_cluster_name
  role_arn = aws_iam_role.aria_eks_iam_role.arn
  version  = var.eks_kubernetes_version

  vpc_config {
    subnet_ids = aws_subnet.aria_eks_subnet[*].id
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Cluster handling.
  # Otherwise, EKS will not be able to properly delete EKS managed EC2 infrastructure such as Security Groups.
  depends_on = [
    aws_iam_role_policy_attachment.aria_eks_cluster_policy_attachment,
    aws_iam_role_policy_attachment.aria_eks_vpc_resource_controller_attachment,
  ]
}
###########################################################################

###########################################################################
# New security group for the EKS cluster                                  #
###########################################################################
resource "aws_security_group" "aria_eks_security_group" {
  name        = var.eks_sg_name
  description = "Cluster communication with worker nodes"
  vpc_id      = aws_vpc.aria_eks_vpc.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "aria_eks_ingress_https" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow any address to communicate with the cluster API Server"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.aria_eks_security_group.id
  to_port           = 443
  type              = "ingress"
}
###########################################################################

###########################################################################
# New Worker groups settings                                              #
###########################################################################
resource "aws_eks_node_group" "aria_eks_node_group" {
  cluster_name    = aws_eks_cluster.aria_eks_cluster.name
  node_group_name = var.eks_node_group_name
  node_role_arn   = aws_iam_role.aria_eks_iam_role.arn
  subnet_ids      = aws_subnet.aria_eks_subnet[*].id

  scaling_config {
    desired_size = 1
    max_size     = 3
    min_size     = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.aria_eks_worker_node_policy,
    aws_iam_role_policy_attachment.aria_eks_cni_policy,
    aws_iam_role_policy_attachment.aria_ec2_container_registry_read_only,
  ]
}

output "endpoint" {
  value = aws_eks_cluster.aria_eks_cluster.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.aria_eks_cluster.certificate_authority[0].data
}