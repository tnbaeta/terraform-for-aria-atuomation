variable "projectId" {
  type        = string
  description = "Choose the GCP project ID"
  default     = "sddc-latam"
}
variable "region" {
  type        = string
  description = "Choose the GCP region"
  default     = "us-east1"
}
variable "gkeVpcName" {
  type        = string
  description = "Choose a name for the new VPC"
  default     = "pocvra-gke-vpc"
}
variable "gkeSubnetName" {
  type        = string
  default     = "pocvra-gke-subnet"
  description = "Choose a name for the new subnet"
}
variable "gkeClusterName" {
  type        = string
  description = "Choose a name for the GKE Cluster"
  default     = "pocvra-gke-cluster"
}
variable "gkePrimaryNodesName" {
  type        = string
  description = "Choose a name for the GKE primary nodes"
  default     = "pocvra-gke-primary-nodes"
}
variable "gkeNumNodes" {
  type        = number
  description = "Choose the number of nodes on the GKE Cluster"
  default     = 2
}

variable "machineType" {
  type        = string
  description = "Choose the type of the instance for the GKE nodes"
  default     = "n1-standard-1"
}
